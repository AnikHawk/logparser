package com.log.parser.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author altamas.haque
 * @since 2/4/20
 */
public class LogParser {
    public static void main(String args[]) throws IOException {
        File file = new File("log.log");
        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;
        HashMap<Integer, Entry> hourMap = new HashMap<Integer, Entry>();

        while ((line = br.readLine()) != null) {
            Pattern uriPattern = Pattern.compile("URI=\\[(.*)\\]");
            Pattern timePattern = Pattern.compile("(\\d+):\\d+:\\d+,\\d+");
            Pattern responseTimePattern = Pattern.compile("time=(\\d+)ms");
            Pattern getPostPattern = Pattern.compile(", (.), time");

            String uri, getPost;
            Integer time, responseTime;

            if (isPatternInSource(timePattern, line)) {
                time = Integer.parseInt(extractPatternFromSource(timePattern, line));

                if (!hourMap.containsKey(time)) {
                    hourMap.put(time, new Entry());
                }

                Entry entry = hourMap.get(time);

                if (isPatternInSource(responseTimePattern, line)) {
                    responseTime = Integer.parseInt(extractPatternFromSource(responseTimePattern, line));
                    entry.incrementResponseTime(responseTime);
                }

                if (isPatternInSource(uriPattern, line)) {
                    uri = extractPatternFromSource(uriPattern, line);
                    entry.updateUriList(uri);
                }

                if (isPatternInSource(getPostPattern, line)) {
                    getPost = extractPatternFromSource(getPostPattern, line);
                    if (getPost.equals("G")) {
                        entry.incrementGet();
                    } else {
                        entry.incrementPost();
                    }
                }
            }
        }
        printOutputFromHashMap(hourMap);
    }

    public static boolean isPatternInSource(Pattern pattern, String source) {
        Matcher matcher = pattern.matcher(source);
        return matcher.find();
    }

    public static String extractPatternFromSource(Pattern pattern, String source) {
        Matcher matcher = pattern.matcher(source);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public static void printOutputFromHashMap(HashMap<Integer, Entry> map) {
        System.out.println(String.format("|%-15s| GET/POST Count | Unique URI Count | Total Response Time |", "Time"));
        for (Integer hour : map.keySet()) {
            Entry entry = map.get(hour);
            Integer startHour = hour;
            Integer endHour = hour + 1;
            Integer getCount = entry.getGetCount();
            Integer postCount = entry.getPostCount();
            Integer uniqueUri = entry.getuniqueUri();
            Integer totalResponseTime = entry.getResponseTime();

            String output = String.format("|%3d.00 - %d.00 | %d/%-11d | %-16d | %17dms |",
                    startHour, endHour, getCount, postCount, uniqueUri, totalResponseTime);

            System.out.println(output);
        }
    }
}
