package com.log.parser.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author altamas.haque
 * @since 2/4/20
 */
public class Entry {
    Integer getCount;
    Integer postCount;
    List<String> uriList;
    Integer responseTime;

    public Entry() {
        getCount = 0;
        postCount = 0;
        uriList = new ArrayList<String>();
        responseTime = 0;
    }

    public Integer getGetCount() {
        return getCount;
    }

    public void setGetCount(Integer getCount) {
        this.getCount = getCount;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public Integer getuniqueUri() {
        return uriList.size();
    }

    public Integer getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Integer responseTime) {
        this.responseTime = responseTime;
    }

    public void incrementGet() {
        this.getCount++;
    }

    public void incrementPost() {
        this.postCount++;
    }

    public void incrementResponseTime(Integer time) {
        this.responseTime += time;
    }

    public void updateUriList(String uri) {
        if (!uriList.contains(uri)) {
            uriList.add(uri);
        }
    }
}
